This demo program shows how you can write a cookie and then delete it.

The index.xhtml jsf page asks for a string and then appends it to whatever us already in the cookie.

The showDetails.xhtml jsf page displays the cookie value and gives you the option of returning to index.xhtml to be able to add another value to the cookie. You can also effectively delete the cookie by writing back to it an empty string.

Unless a cookie has a maxAge it will default to being a session cookie and will be lost when the browser is closed.

A redirect is used to access any page that wants to read cookies. This is necessary because a forward, the default, does not use get any cookies as the request does not generate a new response object.
