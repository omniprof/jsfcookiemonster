package com.kenfogel.jsfcookiemonster.cookiecart;

import java.io.Serializable;
import java.util.ArrayList;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the data structure that manages the string that will be the cookie
 * value. It is also the backing bean for the index.xhtml form.
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class CookieCart implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(CookieCart.class);

    private ArrayList<String> cookieFlavours = new ArrayList<>();

    private String cookieValue;

    /**
     * Returns the current value to be added to the cookie
     *
     * @return
     */
    public String getCookieValue() {
        return cookieValue;
    }

    /**
     * Accepts a value to be added to the cookie and then adds it to the
     * ArrayList
     *
     * @param cookieValue
     */
    public void setCookieValue(String cookieValue) {
        this.cookieValue = cookieValue;
        if (cookieValue != null) {
            cookieFlavours.add(cookieValue);
        }
        LOG.debug("setCookieValue:  " + cookieFlavours.toString());
    }

    /**
     * Empty the ArrayList of all values
     */
    public void clearCart() {
        cookieFlavours.clear();
    }

    /**
     * Return the ArrayList of cokkie vales as a string. Used so that you can
     * display the cookie value
     *
     * @return
     */
    public String getCookieFlavours() {
        return cookieFlavours.toString();
    }

    /**
     * Get the ArrayList of cookie values
     *
     * @return
     */
    public ArrayList<String> getCookies() {
        return cookieFlavours;
    }
}
