package com.kenfogel.jsfcookiemonster.cookieactions;

import com.kenfogel.jsfcookiemonster.cookiecart.CookieCart;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class manages cookies. As a pre-render view it reads the cookies when a
 * page is loaded.
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class CookieManager implements Serializable {

    @Inject
    private CookieCart cookieCart;

    private String theCookies;

    private final Map<String, Object> properties;

    // Default logger is java.util.logging
    private final static Logger LOG = LoggerFactory.getLogger(CookieManager.class);

    /**
     * Need to set maxAge otherwise the cookie is a Session cookie.
     */
    public CookieManager() {
        properties = new HashMap<>();
//        properties.put("domain", "test");
        properties.put("maxAge", 31536000);
//        properties.put("secure", false);
//        properties.put("path", "/");
    }

    /**
     * Read a cookie
     *
     */
    public void readCookies() {
        FacesContext context = FacesContext.getCurrentInstance();
        // Retrieve a specific cookie
        Object my_cookie = context.getExternalContext().getRequestCookieMap().get("CookieKen");
        if (my_cookie != null) {
            LOG.info("readCookies:  Name: " + ((Cookie) my_cookie).getName());
            LOG.info("readCookies:  Value: " + ((Cookie) my_cookie).getValue());
            theCookies = ((Cookie) my_cookie).getValue();
            LOG.debug("readCookies:  theCookies: " + theCookies);
            cookieCart.getCookies().clear();
            cookieCart.getCookies().addAll(Arrays.asList(theCookies.split(",")));
            LOG.debug("readCookies: cookieCart.getCookies(): " + cookieCart.getCookies());
        }
    }

    /**
     * Let's write a cookie!
     * http://docs.oracle.com/javaee/7/api/javax/faces/context/ExternalContext.html#addResponseCookie(java.lang.String,
     * java.lang.String, java.util.Map)
     *
     * @return
     */
    public String writeCookie() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().addResponseCookie("CookieKen", cookieMaker(), properties);
        LOG.debug("writeCookie:  cookieMaker():  " + cookieMaker());
        
        // Without a redirect any new cookie values will be written to the 
        // cookie but won't be retieved until the next round trip.
        return "showCookie?faces-redirect=true";

    }

    /**
     * This method converts the ArrayList of cookie values into a single
     * delimited string.
     *
     * @return
     */
    private String cookieMaker() {
        StringBuilder rawCookie = new StringBuilder();
        LOG.debug("cookieMaker:  cookieCart.getCookieFlavours(): " + cookieCart.getCookieFlavours());
        if (!cookieCart.getCookieFlavours().isEmpty()) {
            for (int x = 0; x < cookieCart.getCookies().size(); ++x) {
                if (cookieCart.getCookies().get(x).length() > 1) {
                    rawCookie.append(cookieCart.getCookies().get(x)).append(",");
                }
            }
        }
        return (rawCookie.toString());
    }

    /**
     * After clearing the list of strings in the cookieCart, it writes the
     * cookies with a null string value effectively emptying the cookie.
     *
     * @return
     */
    public String clearCookies() {
        cookieCart.clearCart();
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().addResponseCookie("CookieKen", null, properties);
        return "index.xhtml?faces-redirect=true";
    }

}
